package com.ikapujirahayu.splashscreen_22;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends AppCompatActivity {
    private  int waktu_loading = 4000;

    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public
            void run() {
                Intent Home = new Intent(MainActivity.this, Home.class);
                startActivity(Home);
                finish();
            }
        },waktu_loading);
    }
}
